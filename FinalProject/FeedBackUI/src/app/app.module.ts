import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import{HttpClientModule} from '@angular/common/http'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeadersComponent } from './Header/headers/headers.component';
import { CreateFormComponent } from './Forms/create-form/create-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import{NgxStarRatingModule} from 'ngx-star-rating';
import { InboxFormComponent } from './Inbox/inbox-form/inbox-form.component'
import { FormService } from './Service/formService';
import { DatePipe } from '@angular/common';
import { ToastrModule } from 'ngx-toastr';
import{BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    HeadersComponent,
    CreateFormComponent,
    InboxFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,HttpClientModule,NgxStarRatingModule,ToastrModule.forRoot(),
    BrowserAnimationsModule
  ],
  providers: [FormService,DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
