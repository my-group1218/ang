import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { formmodel } from '../Models/form';


@Injectable()
export class FormService
{
    formUrl:string='http://localhost:5000/Form';
    formGeturl:string='http://localhost:5000/Form/GetForm';
    formdeleteurl:string='http://localhost:5000/Form/DeleteEmployeeByID/';
    formUpdateurl:string='http://localhost:5000/Form/FormUpdate';
    constructor(private myhttp:HttpClient) { }
  
    listForms:formmodel[]=[]; 
    formData:formmodel=new formmodel(); 
    CreateFormdata(obj:any):Observable<any>
    {
      return this.myhttp.post<any>(this.formUrl,obj);
    }
    getAllforms():Observable<formmodel[]>
    {
      return this.myhttp.get<formmodel[]>(this.formGeturl);
    }
    deleteForm(formId:number)
    {
      return this.myhttp.delete(this.formdeleteurl + formId);
    }
    updateForm()
    {
      return this.myhttp.put(this.formUpdateurl + this.formData.formId,this.formData);
    }
    updateEmployee()
    {
      return this.myhttp.put(`${this.formUpdateurl}/${this.formData.formId}` ,this.formData);
    }
    // getbyid(formId:number)
    // {
    //   return this.myhttp.get(`${this.formGeturl}/${formId}`);
    // }
}