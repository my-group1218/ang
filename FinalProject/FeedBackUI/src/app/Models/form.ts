export class formmodel {
    formId: 0;
    formTitle: string;
    formDescription: string;
    formLink: string;
    formSubmissions: number;
    formPublishedDate: any;
    formStatus: number;
    formClosedDate: any;
}
export class questions {
    questionName: string;
    questionType: number;
}


export class options {

    optionID: number;
    options: string;
}