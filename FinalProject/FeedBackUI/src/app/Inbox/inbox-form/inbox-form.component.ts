import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CreateFormComponent } from 'src/app/Forms/create-form/create-form.component';
import { formmodel } from 'src/app/Models/form';
import { FormService } from 'src/app/Service/formService';

@Component({
  selector: 'app-inbox-form',
  templateUrl: './inbox-form.component.html',
  styleUrls: ['./inbox-form.component.css']
})
export class InboxFormComponent implements OnInit {
  @ViewChild('CreateForm') CreateForm: NgForm

  constructor(public formServiceObj: FormService, public datepipe: DatePipe,private tostr:ToastrService) { }
editMode:boolean=false;

  ngOnInit() {
    this.formServiceObj.getAllforms().subscribe(data => {
      this.formServiceObj.listForms = data;

    })
  }
  onEditForm(formId: number, index: number) {
    this.editMode=true;
    console.log(this.formServiceObj.listForms[index]);
  }
  delete(formId: number) {
    if (confirm('Are you really want to delete this record?')) {
      this.formServiceObj.deleteForm(formId).subscribe(data => {
        this.formServiceObj.getAllforms().subscribe(data => {
          this.formServiceObj.listForms = data;
          this.tostr.success("Record Deleted");;
          console.log('Sucess', 'Record Deleted');
        });
      },
        err => {
        });
    }
  }
}