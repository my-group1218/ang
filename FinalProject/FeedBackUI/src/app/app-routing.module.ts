import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateFormComponent } from './Forms/create-form/create-form.component';
import { InboxFormComponent } from './Inbox/inbox-form/inbox-form.component';


const routes: Routes = [
  { path:'', component: CreateFormComponent},
  { path:'CreateForm/:formId', component: CreateFormComponent},
  { path: 'InboxForm', component: InboxFormComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
