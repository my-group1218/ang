import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { formmodel } from 'src/app/Models/form';
import { FormService } from 'src/app/Service/formService';

@Component({
  selector: 'app-create-form',
  templateUrl: './create-form.component.html',
  styleUrls: ['./create-form.component.css'],
  providers: [DatePipe]
})
export class CreateFormComponent implements OnInit {

  form: FormGroup;
  result: any;
  showformuntitled: Boolean = true;
  openbtn: boolean = true;
  makeInput: boolean = false;
  myDate: any = Date()

  constructor(private datePipe: DatePipe, private tostr: ToastrService, private fb: FormBuilder, private http: Router, public formService: FormService, public routes: ActivatedRoute) {
    this.myDate = this.datePipe.transform(this.myDate, 'yyyy-MM-dd');
  }

  ngOnInit(): void {
    // console.log(this.routes.snapshot.params['formId']);    
    // this.formService.getbyid(this.routes.snapshot.params['formId']).subscribe((result=>
    //   console.log(result)))

    this.creatForm();

  }

  creatForm() {
    this.form = this.fb.group(
      {

        formTitle: new FormControl(null, [Validators.required, Validators.minLength(5)]),
        formDescription: new FormControl(null, [Validators.required, Validators.minLength(5)]),
        formClosedDate: new FormControl(),

        formSubmissions: new FormControl(1),
        formPublishedDate: "2022-07-23T14:52:49.133Z",
        formStatus: 1,
        singleoptionsQuestion: this.fb.array([this.SingleOptionQuestionForm()]),
        checkboxsQuestions: this.fb.array([this.CheckboxForms()]),
        ratingQuestions: this.fb.array([this.ratingQuestionsform()]),
        rankingQuestions: this.fb.array([this.rankingQuestionsform()]),
        singleLineQuestion: this.fb.array([this.singleLineQues()]),
        multiquestion: this.fb.array([this.mulitLineCommentForm()]),
      }
    );

  }
  get formTitle() {
    return this.form.get("formTitle");
  }
  get formDescription() {
    return this.form.get("formDescription");
  }
  get singleoptionsQuestion() {

    return this.form.get("singleoptionsQuestion") as FormArray;
  }

  get checkboxsQuestions() {
    return this.form.get("checkboxsQuestions") as FormArray;
  }

  get ratingQuestions() {
    return this.form.get("ratingQuestions") as FormArray;
  }

  get rankingQuestions() {
    return this.form.get("rankingQuestions") as FormArray;
  }

  get singleLineQuestion() {
    return this.form.get("singleLineQuestion") as FormArray;
  }

  get multiquestion() {
    return this.form.get("multiquestion") as FormArray;
  }

  //////////////////////////////////////////////////////Single Option Type Question///////////////
  SingleOptionQuestionForm() {
    return this.fb.group(
      {
        questionName: [null, Validators.required],
        questionType: [1],
        radio1: [null, Validators.required],
        radio2: [null, Validators.required],
        radio3: [null, Validators.required],
        radio4: [null, Validators.required],

      }
    );
  }
  addNewOptionTypeQuestion() {
    // ''option = true
    this.singleoptionsQuestion.push(this.SingleOptionQuestionForm());
  }
  removeOptionTypeQuestion(i: Required<number>) {
    this.singleoptionsQuestion.removeAt(i);
  }

  ////////////////////////////////////////////////////////// Multiquestion and Multioption Type Questions///////////////
  CheckboxForms() {
    return this.fb.group(
      {
        questionName: [null, Validators.required],
        questionType: 2,
        checkbox1: [null, Validators.required],
        checkbox2: [null, Validators.required],
        checkbox3: [null, Validators.required],
        checkbox4: [null, Validators.required]
      }
    );
  }
  addNewCheckBoxForm() {
    // this.checkoption = true
    this.checkboxsQuestions.push(this.CheckboxForms());
  }
  removeChekbox(i: Required<number>) {
    this.checkboxsQuestions.removeAt(i);
  }

  /////////////////////////////////////////////////////////////////Rating Type Questions///////////////////////////

  ratingQuestionsform() {
    return this.fb.group(
      {
        questionName: [null, Validators.required],
        questionType: [3],

      }
    );
  }

  addNewRatingForm() {
    this.ratingQuestions.push(this.ratingQuestionsform())
  }
  removeRating(i: Required<number>) {
    this.ratingQuestions.removeAt(i);
  }
  //////////////////////////////////////////////////////Ranking Type Questions//////////////////////////////////

  rankingQuestionsform() {
    return this.fb.group(
      {
        questionName: [null, Validators.required],
        questionType: [4],

      });
  }

  addNewRankingForm() {
    this.rankingQuestions.push(this.rankingQuestionsform())
  }
  removeRanking(i: Required<number>) {
    this.rankingQuestions.removeAt(i);
  }

  ////////////////////////////////////////////////////////// Single Line Comment Type Questions ///////////////////
  singleLineQues() {
    return this.fb.group(
      {
        questionName: ['', [Validators.required, Validators.minLength(20)]],
        questionType: [5],

      }
    )
  }

  addNewSingleQuestion() {
    this.singleLineQuestion.push(this.singleLineQues());
  }
  removeSinglForm(i: Required<number>) {
    this.singleLineQuestion.removeAt(i);
  }

  /////////////////////////////////////////////////////////////////Multiline Comment Type Questions////////////////

  mulitLineCommentForm() {
    return this.fb.group(
      {
        questionName: [null, Validators.required],
        questionType: [6],

      }
    );
  }
  AddMultilineComment() {
    // this.multioption = true
    this.multiquestion.push(this.mulitLineCommentForm())
  }
  removeMultilineComment(i: Required<number>) {
    this.multiquestion.removeAt(i);
  }

  //////////////////////////////////////////////////////////////////////These are the function for show and hide the buttons////

  clickOnUntitle() {

    if (this.showformuntitled == false) {
      this.showformuntitled = true

    }
    else {

      this.showformuntitled = false
    }

  }
  buttonOnOF() {
    if (this.openbtn == false) {
      this.openbtn = true
    }
    else {
      this.openbtn = false
    }
  }

  selectedradio() {
    this.makeInput = true
    console.log("radio", '');
  }
  clickoncancle() {
    // window.location.reload();
    this.form.reset();
  }
  ///////////////////////////////////////////////////////////////////

  onSubmit() {

    
    // this.http.navigate(['']);
    // window.location.reload();
    
    if (this.form.valid) {
      console.log(this.form.value);
      this.result = this.form.getRawValue();
      this.tostr.success('Successfully Form Created!')
      this.formService.CreateFormdata(this.result).subscribe(t => {
        console.log(t);
      }, error => {
        console.log(error);
      });
      this.form.reset();
    } else {
      this.tostr.error("Form is Invalid")
    }
  }

}