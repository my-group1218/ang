namespace FeedBackWebApi.DTO
{
    public class OptionsDTO
    {
        public int optionID { get; set; }
        public string[] options { get; set; }
    }
}