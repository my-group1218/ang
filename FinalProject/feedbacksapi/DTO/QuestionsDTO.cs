using System.Collections.Generic;
using FeedBackWebApi.Models;

namespace FeedBackWebApi.DTO
{
    public class QuestionsDTO
    {
        // public int questionID { get; set; }
        public string questionName { get; set; }
        public QuestionType questionType { get; set; }
        public IList<OptionsDTO> Options { get; set; }
    }
}