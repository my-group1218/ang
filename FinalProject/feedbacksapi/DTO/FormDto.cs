using System;
using System.Collections.Generic;
using feedbacksapi.DTO;
using FeedBackWebApi.Models;

namespace form.Dto
{
    public class FormDto
    {
        public string formTitle { get; set; }
        public string formDescription { get; set; }
        public string formLink { get; set; }
        public int formSubmissions { get; set; }
        public DateTime formPublishedDate { get; set; }
        public Status formStatus { get; set; }
        public DateTime formClosedDate { get; set; }
        public IList<CheckboxsQuestion>? checkboxsQuestions { get; set; }
        public IList<Multiquestion>? multiquestion { get; set; }
        public IList<RatingQuestion>? ratingQuestions { get; set; }
        public IList<RankingQuestion>? rankingQuestions { get; set; }
        public IList<SingleLineQuestion>? singleLineQuestion { get; set; }
        public IList<SingleoptionsQuestion>? singleoptionsQuestion { get; set; }
    }

    public class RatingQuestion
    {
        public string questionName { get; set; }
        public QuestionType questionType { get; set; }
    }
    public class RankingQuestion
    {
        public string questionName { get; set; }
        public QuestionType questionType { get; set; }
    }

    public class SingleLineQuestion
    {
        public string questionName { get; set; }
        public QuestionType questionType { get; set; }
    }
    public class SingleoptionsQuestion
    {
        public string questionName { get; set; }
        public QuestionType questionType { get; set; }
        public string radio1 { get; set; }
        public string radio2 { get; set; }
        public string radio3 { get; set; }
        public string radio4 { get; set; }
    }
    public class Multiquestion
    {
        public string questionName { get; set; }
        public QuestionType questionType { get; set; }
    }
}