using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FeedBackWebApi.Models;

namespace feedbacksapi.DTO
{
    public class CheckboxsQuestion
    {
         public string questionName { get; set; }
        public QuestionType questionType { get; set; }
        public string checkbox1 { get; set; }
        public string checkbox2 { get; set; }
        public string checkbox3 { get; set; }
        public string checkbox4 { get; set; }
    }
}