using System.Linq;

using System.Collections.Generic;
namespace FeedBackWebApi.DTO
{
    public class UserDto
    {
        public string userName { get; set; }
        public int formId { get; set; }
        public List<UserResponseDTO> UserResponse { get; set; }

    }
    public class UserResponseDTO
    {
        public int quesno { get; set; }
        public string[] answer { get; set; }
    }

}
