using Microsoft.AspNetCore.Mvc;
using FeedBackWebApi.Services.Interfaces;
using FeedBackWebApi.DTO;

namespace FeedBackWebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserResponseController : ControllerBase
    {
        private readonly IUserService _iuserservice;
        public UserResponseController(IUserService iuserservice)
        {
            this._iuserservice = iuserservice;
        }
         [HttpPost]
        [Route("Createrespomse")]
        public IActionResult createresponse(UserDto userDto)
        {
            return Ok(_iuserservice.funcreateresponse(userDto));
        }

        
    }
}