using FeedBackWebApi.DTO;
using FeedBackWebApi.Models;
using FeedBackWebApi.Services.Interfaces;
using form.Dto;
using Microsoft.AspNetCore.Mvc;

namespace FeedBackWebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FormController : ControllerBase
    {
        private readonly IFormService _iformserviceObject;
        public FormController(IFormService _iformserviceObject)
        {
            this._iformserviceObject = _iformserviceObject;

        }
         [HttpPost]
        public IActionResult createForm(FormDto formDto)
        {
            return Ok(_iformserviceObject.CreateForm(formDto));
        }
        [HttpGet]
        [Route("GetForm")]
        public IActionResult getform()
        {
            return Ok(_iformserviceObject.GetForms());
        }

        [HttpPut]
        [Route(("FormUpdate/{formId}"))]
        public IActionResult UpdateForms(Form forms)
        {
            return Ok(_iformserviceObject.UpdateForms(forms));
        }

        [HttpDelete("DeleteEmployeeByID/{formId}")]
        public IActionResult DeleteEmployee(int formId)
        {
            return Ok(this._iformserviceObject.deleteforms(formId));
        }

    }
}
