using System;
using System.Collections.Generic;

namespace FeedBackWebApi.Models
{
    public class Form
    {
        public Form()
        {
            Questions = new HashSet<Questions>();
            UserResponse = new HashSet<UserResponse>();
        }

        public int formId { get; set; }
        public string formTitle { get; set; }
        public string formDescription { get; set; }
        public string formLink { get; set; }
        public int formSubmissions { get; set; }
        public DateTime formPublishedDate { get; set; }
        public Status formStatus { get; set; }
        public DateTime formClosedDate { get; set; }


        //Navigation Property
        public ICollection<Questions> Questions { get; set; }
        public ICollection<UserResponse> UserResponse { get; set; }
    }
    [Flags]
    public enum Status
    {
        published = 1, drafted = 2, closed = 3
    }
}