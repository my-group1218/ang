using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using feedbacksapi.Models;

namespace FeedBackWebApi.Models

{
    public class UserResponse
    {
         //For making primary key.
         public int userRespId { get; set; }
        // public string[] options { get; set; }
        // public int questionNo { get; set; }
        public int userId { get; set; }
        public User User { get; set; }
        public Form Form{get;set;}
        public int formId{get;set;}
        public ICollection<userAnswer> userAnswer{get;set;} 
    }
}