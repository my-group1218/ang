using System.Collections.Generic;

namespace FeedBackWebApi.Models
{
    public class Questions
    {
        public Questions()
        {
            Options=new HashSet<Options>();
        }

        public int questionID{get;set;}
        public string questionName{get;set;}
        public QuestionType questionType{get;set;}
        public int formId{get;set;}
        public Form Form{get;set;} //Navigation Property for Form.

        public ICollection<Options> Options{get;set;} //One Question have multiple option for that we are using Navigation property for making relation between them.
        
    }
    public enum QuestionType
    {
        singleChoice=1,
        multiChoice=2,
        rating=3,
        ranking=4,
        singleLineAnswer=5,
        multiLineAnswer=6
    }
}