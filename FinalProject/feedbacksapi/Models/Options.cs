namespace FeedBackWebApi.Models
{
    public class Options
    {
        public int optionID { get; set; }
        public string options { get; set; }
        public int questionID { get; set; }
        public Questions Questions { get; set; }//Navigation property of Question class.        

    }
}