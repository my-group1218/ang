using System.Collections.Generic;
namespace FeedBackWebApi.Models
{
    public class User
    {
        // public User()
        // {
        //     UserResponse = new HashSet<UserResponse>();
        // }
        public int userId { get; set; }
        public string userName { get; set; }
        public string Email{get;set;}
        // public int formId { get; set; }
        // public Form Form { get; set; } //Navigation property for Form.
        public UserResponse UserResponse { get; set; }//Navigation property for User Response.

    }
}