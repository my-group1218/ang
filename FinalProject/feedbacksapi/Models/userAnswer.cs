using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FeedBackWebApi.Models;

namespace feedbacksapi.Models
{
    public class userAnswer
    {
        public int useranswerId{get;set;}
        public string[] answer { get; set; }
        public int questionNo { get; set; }
        public UserResponse UserResponse {get;set;}
        public int userRespId{get;set;}
        
    }
}