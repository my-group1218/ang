using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FeedBackWebApi.Models.Mapper
{
    public class FormMapper
    {
        public FormMapper(EntityTypeBuilder<Form> entity)
        {
            entity.HasKey(m => m.formId);
            // entity.Property(m=>m.formId).ValueGeneratedOnAdd();
            entity.Property(m => m.formTitle).IsRequired();
            entity.Property(m => m.formDescription).IsRequired();
            entity.HasMany(m => m.Questions).WithOne(m => m.Form);
            entity.HasMany(m => m.UserResponse).WithOne(m => m.Form);


        }
    }
}