using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FeedBackWebApi.Models.Mapper
{
    public class UserMapper
    {
         public UserMapper(EntityTypeBuilder<User> entity)
        {
            entity.HasKey(m=>m.userId);
            entity.HasAlternateKey(m=>m.Email);
            entity.Property(m=>m.Email).IsRequired();
            // entity.HasOne(m=>m.UserResponse).WithOne(m=>m.User);
            entity.HasOne(i=>i.UserResponse).WithOne(i=>i.User);

        }
    }
}