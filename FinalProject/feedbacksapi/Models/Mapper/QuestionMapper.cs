using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FeedBackWebApi.Models.Mapper
{
    public class QuestionMapper
    {
        public QuestionMapper(EntityTypeBuilder<Questions> entity)
        {
            entity.HasKey(m => m.questionID);
            // entity.Property(m=>m.questionID).ValueGeneratedOnAdd();
            entity.HasOne(m => m.Form).WithMany(m => m.Questions).HasForeignKey(m => m.formId);
        }
    }
}