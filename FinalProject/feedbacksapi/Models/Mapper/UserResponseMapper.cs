using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FeedBackWebApi.Models.Mapper
{
    public class UserResponseMapper
    {
        public UserResponseMapper(EntityTypeBuilder<UserResponse> entity)
        {
            entity.HasKey(m => m.userRespId);
            entity.HasAlternateKey(c => new { c.userId, c.formId });
            entity.HasOne(m => m.Form).WithMany(m => m.UserResponse).HasForeignKey(m => m.formId);
            entity.HasOne(i=>i.User).WithOne(i=>i.UserResponse).HasForeignKey<User>(d=>d.userId);
             entity.HasMany(m => m.userAnswer).WithOne(m => m.UserResponse);


            // entity.HasOne(m => m.User).WithOne(m => m.UserResponse).HasForeignKey(d => d.userId);
        }
    }
}