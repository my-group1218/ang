using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace feedbacksapi.Models.Mapper
{
    public class userAnswerMapper
    {
        public userAnswerMapper(EntityTypeBuilder<userAnswer> entity)
        {
            entity.HasKey(m => m.useranswerId);
            entity.HasOne(m => m.UserResponse).WithMany(m => m.userAnswer).HasForeignKey(m => m.userRespId);

        }
    }
}