using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FeedBackWebApi.Models.Mapper
{
    public class OptionMapper
    {
        public OptionMapper(EntityTypeBuilder<Options> entity)
        {
            entity.HasKey(m => m.optionID);

            entity.HasOne(m => m.Questions).WithMany(m => m.Options).HasForeignKey(m => m.questionID);
        }
    }
}