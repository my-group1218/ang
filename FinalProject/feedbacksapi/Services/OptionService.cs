using FeedBackWebApi.EfCore;
using FeedBackWebApi.Models;
using FeedBackWebApi.Services.Interfaces;

namespace FeedBackWebApi.Services
{
    public class OptionService : IOptionService
    {
        ApplicationDbContext appDbContext;
        public OptionService(ApplicationDbContext _appDbContext)
        {
            appDbContext=_appDbContext;
        }
        public int CreateOptions(Options optionObj)
        {
            appDbContext.Add(optionObj);
             appDbContext.SaveChanges();
             return optionObj.optionID;
        }
    }
}