using FeedBackWebApi.Models;

namespace FeedBackWebApi.Services.Interfaces
{
    public interface IOptionService
    {
        public int CreateOptions(Options optionObj); 
    }
}