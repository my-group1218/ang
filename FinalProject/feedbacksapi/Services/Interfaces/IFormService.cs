using System.Collections.Generic;
using FeedBackWebApi.DTO;
using FeedBackWebApi.Models;
using form.Dto;

namespace FeedBackWebApi.Services.Interfaces
{
    public interface IFormService
    {
        public int CreateForm(FormDto formDto);
        public List<Form> GetForms();
        public int UpdateForms(Form form);
        public int deleteforms(int formId);

        //put operation
    }
}