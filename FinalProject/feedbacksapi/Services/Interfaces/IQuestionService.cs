using FeedBackWebApi.Models;

namespace FeedBackWebApi.Services.Interfaces
{
    public interface IQuestionService
    {
        public int CreateQuestion(Questions questionObj);
    }
}