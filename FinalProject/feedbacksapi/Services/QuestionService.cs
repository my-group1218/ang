using FeedBackWebApi.EfCore;
using FeedBackWebApi.Models;
using FeedBackWebApi.Services.Interfaces;

namespace FeedBackWebApi.Services
{
    public class QuestionService : IQuestionService
    {
        ApplicationDbContext appDbContext;
        public QuestionService(ApplicationDbContext _appDbContext)
        {
            appDbContext=_appDbContext;
        }
        public int CreateQuestion(Questions questionObj)
        {
            appDbContext.Add(questionObj);
             appDbContext.SaveChanges();
             return questionObj.questionID;
        }
    }
}