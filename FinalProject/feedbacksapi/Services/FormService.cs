using System.Collections.Generic;
using System.Linq;
using System;
using FeedBackWebApi.EfCore;
using FeedBackWebApi.Models;
using Microsoft.EntityFrameworkCore;
using FeedBackWebApi.Services.Interfaces;
using form.Dto;

namespace FeedBackWebApi.Services
{
    public class FormService : IFormService
    {
        private readonly ApplicationDbContext appDbContext;
        public FormService(ApplicationDbContext _appDbContext)
        {
            appDbContext = _appDbContext;
        }

        public int CreateForm(FormDto formDto)
        {
            var Form = new Form();
            {
                Form.formTitle = formDto.formTitle;
                Form.formDescription = formDto.formDescription;
                Form.formStatus = formDto.formStatus;
                Form.formPublishedDate = System.DateTime.Now;
                Form.formClosedDate= formDto.formClosedDate;
                Form.formLink = "http://localhost:5000/Form/GetForm";
                appDbContext.Forms.Add(Form);
                appDbContext.SaveChanges();
                if (formDto.singleoptionsQuestion != null)
                {
                    foreach (var mcqquestion in formDto.singleoptionsQuestion)
                    {
                        var question = new Questions();
                        {
                            question.questionName = mcqquestion.questionName;
                            question.questionType = mcqquestion.questionType;
                            question.formId = Form.formId;
                            var option1 = new Options();
                            {
                                option1.questionID = question.questionID;
                                option1.options = mcqquestion.radio1;
                            }
                            question.Options.Add(option1);
                            var option2 = new Options();
                            {
                                option2.questionID = question.questionID;
                                option2.options = mcqquestion.radio2;
                            }
                            question.Options.Add(option2);
                            var option3 = new Options();
                            {
                                option3.questionID = question.questionID;
                                option3.options = mcqquestion.radio3;
                            }
                            question.Options.Add(option3);
                            var option4 = new Options();
                            {
                                option4.questionID = question.questionID;
                                option4.options = mcqquestion.radio2;
                            }
                            question.Options.Add(option4);
                            appDbContext.Question.Add(question);
                        }
                    }
                    if (formDto.checkboxsQuestions != null)
                    {

                        foreach (var msqquestion in formDto.checkboxsQuestions)
                        {

                            var Msqquestion = new Questions();
                            {
                                Msqquestion.questionName = msqquestion.questionName;
                                Msqquestion.questionType = msqquestion.questionType;
                                Msqquestion.formId = Form.formId;
                                var checkbox1 = new Options();
                                {
                                    checkbox1.questionID = Msqquestion.questionID;
                                    checkbox1.options = msqquestion.checkbox1;
                                }
                                Msqquestion.Options.Add(checkbox1);
                                var checkbox2 = new Options();
                                {
                                    checkbox2.questionID = Msqquestion.questionID;
                                    checkbox2.options = msqquestion.checkbox2;
                                }
                                Msqquestion.Options.Add(checkbox2);
                                var checkbox3 = new Options();
                                {
                                    checkbox3.questionID = Msqquestion.questionID;
                                    checkbox3.options = msqquestion.checkbox3;
                                }
                                Msqquestion.Options.Add(checkbox3);
                                var checkbox4 = new Options();
                                {
                                    checkbox4.questionID = Msqquestion.questionID;
                                    checkbox4.options = msqquestion.checkbox4;
                                }
                                Msqquestion.Options.Add(checkbox4);
                            }
                            appDbContext.Question.Add(Msqquestion);
                            // return appDbContext.SaveChanges();
                        }

                    }
                    if (formDto.rankingQuestions != null)
                    {
                        foreach (var rankingquestion in formDto.rankingQuestions)
                        {
                            var Rankquestion = new Questions();
                            {
                                Rankquestion.questionName = rankingquestion.questionName;
                                Rankquestion.questionType = rankingquestion.questionType;
                                Rankquestion.formId = Form.formId;
                            }
                            appDbContext.Question.Add(Rankquestion);
                        }
                    }
                    if (formDto.ratingQuestions != null)
                    {
                        foreach (var ratingquestion in formDto.ratingQuestions)
                        {
                            var Ratingquestion = new Questions();
                            {
                                Ratingquestion.questionName = ratingquestion.questionName;
                                Ratingquestion.questionType = ratingquestion.questionType;
                                Ratingquestion.formId = Form.formId;
                            }
                            appDbContext.Question.Add(Ratingquestion);
                        }
                    }
                    if (formDto.singleLineQuestion != null)
                    {
                        foreach (var singlelinequestion in formDto.singleLineQuestion)
                        {
                            var SingleLinequestion = new Questions();
                            {
                                SingleLinequestion.questionName = singlelinequestion.questionName;
                                SingleLinequestion.questionType = singlelinequestion.questionType;
                                SingleLinequestion.formId = Form.formId;
                            }
                            appDbContext.Question.Add(SingleLinequestion);
                        }
                    }
                    if (formDto.multiquestion != null)
                    {
                        foreach (var multilinequestion in formDto.multiquestion)
                        {
                            var MultiLinequestion = new Questions();
                            {
                                MultiLinequestion.questionName = multilinequestion.questionName;
                                MultiLinequestion.questionType = multilinequestion.questionType;
                                MultiLinequestion.formId = Form.formId;
                            }
                            appDbContext.Question.Add(MultiLinequestion);
                        }
                    }

                }

            }
            return appDbContext.SaveChanges();
        }



        public List<Form> GetForms()
        {
            var form = appDbContext.Forms
        .Include(i => i.Questions)
           .ThenInclude(i => i.Options)
        .ToList();
            return form;
        }

        public int UpdateForms(Form form)
        {
            appDbContext.Forms.Update(form);
            return appDbContext.SaveChanges();
        }
        public int deleteforms(int formId)
        {
            var del = appDbContext.Forms.Find(formId);
            appDbContext.Forms.Remove(del);
            return appDbContext.SaveChanges();

        }




    }
}