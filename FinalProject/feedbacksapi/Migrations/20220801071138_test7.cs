﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace feedbacksapi.Migrations
{
    public partial class test7 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Forms",
                columns: table => new
                {
                    formId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    formTitle = table.Column<string>(type: "text", nullable: false),
                    formDescription = table.Column<string>(type: "text", nullable: false),
                    formLink = table.Column<string>(type: "text", nullable: true),
                    formSubmissions = table.Column<int>(type: "integer", nullable: false),
                    formPublishedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    formStatus = table.Column<int>(type: "integer", nullable: false),
                    formClosedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Forms", x => x.formId);
                });

            migrationBuilder.CreateTable(
                name: "Question",
                columns: table => new
                {
                    questionID = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    questionName = table.Column<string>(type: "text", nullable: true),
                    questionType = table.Column<int>(type: "integer", nullable: false),
                    formId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Question", x => x.questionID);
                    table.ForeignKey(
                        name: "FK_Question_Forms_formId",
                        column: x => x.formId,
                        principalTable: "Forms",
                        principalColumn: "formId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserResponses",
                columns: table => new
                {
                    userRespId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    userId = table.Column<int>(type: "integer", nullable: false),
                    formId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserResponses", x => x.userRespId);
                    table.UniqueConstraint("AK_UserResponses_userId_formId", x => new { x.userId, x.formId });
                    table.ForeignKey(
                        name: "FK_UserResponses_Forms_formId",
                        column: x => x.formId,
                        principalTable: "Forms",
                        principalColumn: "formId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Option",
                columns: table => new
                {
                    optionID = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    options = table.Column<string>(type: "text", nullable: true),
                    questionID = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Option", x => x.optionID);
                    table.ForeignKey(
                        name: "FK_Option_Question_questionID",
                        column: x => x.questionID,
                        principalTable: "Question",
                        principalColumn: "questionID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "userAnswer",
                columns: table => new
                {
                    useranswerId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    answer = table.Column<string[]>(type: "text[]", nullable: true),
                    questionNo = table.Column<int>(type: "integer", nullable: false),
                    UserResponseuserRespId = table.Column<int>(type: "integer", nullable: true),
                    userRespId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_userAnswer", x => x.useranswerId);
                    table.ForeignKey(
                        name: "FK_userAnswer_UserResponses_UserResponseuserRespId",
                        column: x => x.UserResponseuserRespId,
                        principalTable: "UserResponses",
                        principalColumn: "userRespId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    userId = table.Column<int>(type: "integer", nullable: false),
                    userName = table.Column<string>(type: "text", nullable: true),
                    Email = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.userId);
                    table.UniqueConstraint("AK_Users_Email", x => x.Email);
                    table.ForeignKey(
                        name: "FK_Users_UserResponses_userId",
                        column: x => x.userId,
                        principalTable: "UserResponses",
                        principalColumn: "userRespId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Option_questionID",
                table: "Option",
                column: "questionID");

            migrationBuilder.CreateIndex(
                name: "IX_Question_formId",
                table: "Question",
                column: "formId");

            migrationBuilder.CreateIndex(
                name: "IX_userAnswer_UserResponseuserRespId",
                table: "userAnswer",
                column: "UserResponseuserRespId");

            migrationBuilder.CreateIndex(
                name: "IX_UserResponses_formId",
                table: "UserResponses",
                column: "formId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Option");

            migrationBuilder.DropTable(
                name: "userAnswer");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Question");

            migrationBuilder.DropTable(
                name: "UserResponses");

            migrationBuilder.DropTable(
                name: "Forms");
        }
    }
}
