using FeedBackWebApi.EfCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace FeedBackWebApi.DynamicMigration
{
    public static class DynamicMigrations        //Class must be static
    {
        public static void Migration(this IApplicationBuilder app) //Function must be static for extension method.
        {
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                serviceScope.ServiceProvider.GetService<ApplicationDbContext>().Database.Migrate();
            }
        }
    }
}