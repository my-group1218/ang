using FeedBackWebApi.Models;
using FeedBackWebApi.Models.Mapper;
using Microsoft.EntityFrameworkCore;

namespace FeedBackWebApi.EfCore
{
    public class ApplicationDbContext : DbContext
    {
        //Constructor Chaining.
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)// //Using FluentApi for making constraints and required fields.
        {
            new FormMapper(modelBuilder.Entity<Form>());
            new QuestionMapper(modelBuilder.Entity<Questions>());
            new OptionMapper(modelBuilder.Entity<Options>());
            new UserMapper(modelBuilder.Entity<User>());
            new UserResponseMapper(modelBuilder.Entity<UserResponse>());




        }

        //Creating Table in Database by using DbSet<>.
        public DbSet<Form> Forms { get; set; }
        public DbSet<Questions> Question { get; set; }
        public DbSet<Options> Option { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserResponse> UserResponses { get; set; }

    }
}